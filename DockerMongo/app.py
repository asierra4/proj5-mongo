import flask
import os
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
#import config
import logging
from pymongo import MongoClient

###
# Globals
###
app = flask.Flask(__name__)
#CONFIG = config.configuration()
app.secret_key = " a " 

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route('/')
def todo():
    return render_template('todo.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
#    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/_calc_times")
def _calc_times():
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    b_date = request.args.get('b_date')
    b_time = request.args.get('b_time')
    brev = request.args.get('brev', type=int)

    date_string = b_date + ' ' + b_time + ':00'

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, brev, date_string)
    close_time = acp_times.close_time(km, brev, date_string)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/new', methods=['POST'])
def new():
    db.tododb.remove({})
    l = bool((request.form.getlist('open')[0]) )
    item_doc = {
        'name': request.form.getlist('open'),
        'description': request.form.getlist("close")
    }
    if (not l):
        flask.flash("ERROR: Trying to add empty data")
      
    else:
        db.tododb.insert(item_doc)

    return redirect(url_for('todo'))

@app.route('/display', methods=['POST'])
def display():
    _items = db.tododb.find()
    if (db.tododb.count() == 0):
        flask.flash("ERROR: Trying to display empty data")
        return render_template('todo.html')
    else:
        items = [item for item in _items]
        return render_template('todo.html', items=items)


#app.debug = CONFIG.DEBUG
#if app.debug:
#    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
 #   print("Opening for global access on port {}".format(CONFIG.PORT))
 #   app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
    app.run(host="0.0.0.0", debug=True)

